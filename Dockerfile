# podman build . -t lercher:sfs && podman run -p 8080:8080 lercher:sfs

# --------------------------------- build image
FROM golang:alpine AS build
WORKDIR /go/sfs
ADD go.mod *.go dir.html ./
# we don't want to use C based dynamic libraries, so we disable CGO
ENV CGO_ENABLED=0
RUN go version
# download only
RUN go get -d
# recompile (-a) all dependencies and the sources with CGO disabled
# -installsuffix cgo puts intermediaries to a different folder
# addresses "bash: ./sfs: Datei oder Verzeichnis nicht gefunden" on runing the image 
RUN go build -a -installsuffix cgo
RUN ls -l

RUN apk add file
RUN file sfs
# right (statically linked):
#   sfs: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, Go BuildID=EQmnYdFj9_1ih5Ly8Yi9/A4dXfNL2fOVuIUNtZNWS/5X0nev15yZwE26vtggAf/CL0Xxw6IfT6rU-L9EYRZ, not stripped
# wrong (dynamically linked), without disabled CGO:
#   sfs: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib/ld-musl-x86_64.so.1, Go BuildID=XKo03OGPpnfOtGeGjoEv/A4dXfNL2fOVuIUNtZNWS/5X0nev15yZwE26vtggAf/Ar1thqvRhnY9QqbtTdeZ, not stripped

# -------------------------------- deployment image
FROM scratch
# FROM alpine:latest
LABEL description="serves static files via http"
COPY --from=build /go/sfs/sfs /
WORKDIR /public
ADD index.html .
EXPOSE 8080
ENTRYPOINT [ "/sfs" ]
CMD [ "-path", "/public", "-port", ":8080", "-prefix", "/" ]
