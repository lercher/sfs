# sfs - Template

To use the image, copy this [Dockerfile](Dockerfile),
`ADD ...` your html and other resources and
then build ans start the new container
with e.g.

````sh
podman build . -t sfs:latest
podman run -p 8080:8080 sfs:latest
````
