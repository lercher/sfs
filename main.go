package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"time"
	"path"
	"strings"
)

const sfsVersion = `1.0.3`

var (
	flagVerbose = flag.Bool("v", false, "verbose mode")
	flagPath    = flag.String("path", "public", "`path` to a directory of files to publish")
	flagPort    = flag.String("port", ":8080", "address:port to publish the http server")
	flagPrefix  = flag.String("prefix", "/", "virtual directory to publish the files to")
)

func main() {
	log.Println("This is", os.Args[0], sfsVersion, "(C) 2019 by Martin Lercher")
	flag.Parse()

	prefix := path.Clean(*flagPrefix)
	if !strings.HasSuffix(prefix, "/") {
		prefix = prefix + "/"
	}

	fs := http.FileServer(http.Dir(*flagPath))
	tfs := http.StripPrefix(prefix, fs)
	if *flagVerbose {
		http.HandleFunc(prefix, func(w http.ResponseWriter, r *http.Request) {
			t := time.Now()
			srw := &statusResponseWriter{w, 0}
			defer func() { 
				log.Printf("%12v %3v %v", time.Now().Sub(t), srw.status, r.URL)
			}()
			tfs.ServeHTTP(srw, r)
		})
		log.Printf(`Serving and logging static files from '%s' to 'http://%s%s'`, *flagPath, *flagPort, prefix)
	} else {
		http.Handle(prefix, tfs)
		log.Printf(`Serving static files from '%s' to 'http://%s%s'`, *flagPath, *flagPort, prefix)
	}
	log.Fatalln(http.ListenAndServe(*flagPort, nil))
}

type statusResponseWriter struct {
	http.ResponseWriter
	status int
}

func (w *statusResponseWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}