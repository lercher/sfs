# sfs ![Build Status](https://gitlab.com/lercher/sfs/badges/master/pipeline.svg) ![License MIT](https://img.shields.io/badge/license-MIT-green.svg)

Simple File Server - Serves static files via http

## Intended Usage

Have a look into the [template](template/readme.md)
subdir. The docker image is a download of round-about
4MB, just extend it with your static files and
run the new container.

## Resource Usage under 64bit Linux

This is, what `top` says:

````text
  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
  424 lercher   20   0  479592  13428   4952 S 0,000 0,167   0:00.01 sfs  
````

13,428KiB of `RES - anything occupying physical memory` means 13.5 megs of all sort of RAM.

And the System Monitor GUI app states 8.5M mem
plus 5.0M common mem, which practically is the same
amount.

## Starting the Gitlab image

Using `podman` instead of `docker`:

````sh
podman run -p 8080:8080 registry.gitlab.com/lercher/sfs:latest
````

## Local Image Build

Using `podman` instead of `docker`:

````sh
podman build . -t lercher:sfs --format docker
````

**Note:** We need to add `--format docker` instead of the
default `--format oci`, if we intend to push a locally
created image to github.com and avoid to see `null bytes` and created `right now`
in GitLab's Registry view.
See [podman's man page](https://www.mankier.com/1/podman-build)
and this [issue #4349](https://gitlab.com/gitlab-com/support-forum/issues/4349) for details.

To inspect the image's contents see [this stackoverflow.com answer](https://stackoverflow.com/a/53481010/2219668).

````sh
podman create --name="xx" lercher:sfs
podman export xx | tar t
podman rm xx
````

### Starting the local image

````sh
podman run -p 8080:8080 lercher:sfs
````

after getting [http://localhost:8080/](http://localhost:8080/) it logs:

````text
2019/04/30 18:59:12 This is /sfs 1.0.1 (C) 2019 by Martin Lercher
2019/04/30 18:59:12 Serving and logging static files from '/public' to 'http://:8080/'
````

## Building and Pushing Locally

To build:

````sh
podman build --no-cache -t registry.gitlab.com/lercher/sfs . --format docker
````

**Note:** `--format docker` is required for GitLab's
Registry view to recognize the image's metadata
properly. Otherwise podman creates the image in OCI
format, which displays as `null bytes` at size and
`right now` at created.

To push, first login and then push:

````sh
podman login registry.gitlab.com
podman push  registry.gitlab.com/lercher/sfs
````
